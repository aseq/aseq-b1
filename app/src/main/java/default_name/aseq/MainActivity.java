package default_name.aseq;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    String email;
    String pwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final EditText mEmail = (EditText)findViewById(R.id.emailEditText);
        final EditText mPwd = (EditText)findViewById(R.id.passwordEditText);


        Button loginBtn = (Button)findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkCred(mEmail.getText().toString(), mPwd.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Welcome", Toast.LENGTH_SHORT).show();

                    Intent next = new Intent(getApplicationContext(), Dash.class);
                    final AuthObj authObj = new AuthObj(mEmail.getText().toString());
                    next.putExtra("authObj", authObj);
                    startActivity(next);
                } else {
                    Toast.makeText(getApplicationContext(), "ceva nu e bine :(", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button signUp = (Button) findViewById(R.id.signUpBtn);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent next = new Intent(getApplicationContext(), Signup.class);
                startActivity(next);
            }
        });
    }

    protected boolean checkCred(String email, String pwd){
        if(email.equals("test") && pwd.equals("test")) return true;
        else return false;
    }
}
