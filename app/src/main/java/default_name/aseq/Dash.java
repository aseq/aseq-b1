package default_name.aseq;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

public class Dash extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash);

        AuthObj authObj = (AuthObj) getIntent().getParcelableExtra("authObj");
        Log.w("Mine", authObj.getEmail());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        // Initial Fragment
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new ProfileFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_profile);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_profile:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ProfileFragment()).commit();
                break;
            case R.id.nav_pending:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new PendingFragment()).commit();
                break;
            case R.id.nav_history:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new HistoryFragment()).commit();
            case R.id.exampleMenuItem1:
                Intent next = new Intent(getApplicationContext(), Cornucopia.class);
                startActivity(next);
                break;
            case R.id.exampleQuiz:
                Intent next2 = new Intent(getApplicationContext(), Quiz.class);
                startActivity(next2);
                break;
            case R.id.exampleEvaluareQuiz:
                Intent next3 = new Intent(getApplicationContext(), EvaluareQuiz.class);
                startActivity(next3);
                break;
            case R.id.exampleHighscore:
                Intent next5 = new Intent(getApplicationContext(), Highscore.class);
                startActivity(next5);
                break;
            case R.id.exampleGroupHighscore:
                Intent next6 = new Intent(getApplicationContext(), GroupHighscore.class);
                startActivity(next6);
                break;
            case R.id.exampleProfile:
                Intent next7 = new Intent(getApplicationContext(), Profile.class);
                startActivity(next7);
                break;
            case R.id.exampleSettings:
                Intent next4 = new Intent(getApplicationContext(), Settings.class);
                startActivity(next4);
                break;

            case R.id.entitySearch:
                Intent next8 = new Intent(getApplicationContext(), EntitySearch.class);
                startActivity(next8);
                break;
            case R.id.reports:
                Intent next9 = new Intent(getApplicationContext(), Reports.class);
                startActivity(next9);
                break;
            case R.id.quizCreator:
                Intent next10 = new Intent(getApplicationContext(), QuizCreator.class);
                startActivity(next10);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
