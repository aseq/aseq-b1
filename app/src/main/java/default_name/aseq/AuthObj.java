package default_name.aseq;

import android.os.Parcel;
import android.os.Parcelable;

public class AuthObj implements Parcelable {

    private String userId;
    private String email;
    private String userName;
    private String type;



    public AuthObj(String email) {
        this.email = email;
        this.type = "true";
    }


    protected AuthObj(Parcel in) {
        userId = in.readString();
        email = in.readString();
        userName = in.readString();
        type = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(email);
        dest.writeString(userName);
        dest.writeString(type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AuthObj> CREATOR = new Creator<AuthObj>() {
        @Override
        public AuthObj createFromParcel(Parcel in) {
            return new AuthObj(in);
        }

        @Override
        public AuthObj[] newArray(int size) {
            return new AuthObj[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getUserName() {
        return userName;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

}
